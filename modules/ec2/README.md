## EC2
This module is used to show how DDNS works.


```mermaid
graph TD
subgraph A[dev-vpc]
  1[Availability Zone 1] <--> a
  2[Availability Zone 2]
  a[dev-ec2<br>DNSName:<br>dev-ddns-lab]
end
subgraph B[test-vpc]
  3[Availability Zone 1] <--> b
  4[Availability Zone 2]
  b[test-ec2<br>DNSName:<br>test-ddns-lab]
end
subgraph C[prod-vpc]
  5[Availability Zone 1] <--> c
  6[Availability Zone 2]
  c[prod-ec2<br>DNSName:<br>prod-ddns-lab]
end
subgraph D[Transit Gateway]
  subgraph E[Multi-VPC Route Table]
    7[VPC1 Attachment] <--> 1
    7[VPC1 Attachment] <--> 2
    8[VPC2 Attachment] <--> 3
    8[VPC2 Attachment] <--> 4
    9[VPC3 Attachment] <--> 5
    9[VPC4 Attachment] <--> 6
  end
end

style A fill:#f7edd0
style B fill:#f7edd0
style C fill:#f7edd0
style D fill:#d8e0e6
style 1 fill:#d8e6d8
style 2 fill:#d8e6d8
style 3 fill:#d8e6d8
style 4 fill:#d8e6d8
style 5 fill:#d8e6d8
style 6 fill:#d8e6d8
```
