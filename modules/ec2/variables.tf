## EC2 Variables
variable "vpc_id" {
  description = "vpc_id"
  type        = string
}

variable "ec2_name" {
  type = string
}

variable "public_subnet_id" {
  type = string
}