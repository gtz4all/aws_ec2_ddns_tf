import json
import boto3
import re
import uuid
import time
import random
from datetime import datetime
from botocore.exceptions import ClientError

print(('Loading function ' + datetime.now().strftime("%m-%d-%Y %H:%M:%S")))

DateTime = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
route53 = boto3.client('route53')
dynamodb_client = boto3.client('dynamodb')
dynamodb_resource = boto3.resource('dynamodb')
ec2_resource = boto3.resource('ec2')
ec2_client = boto3.client('ec2')

'''Adjust these variables to fit your environment'''
switchRole = "sts-ddns-lambda-role"
vpcTypeTag = 'SharedVPC'
dnsTag = 'DNSName'

def lambda_handler(event, context):

    #set event variables
    account = event['account']
    region = event['region']
    # Assume Role on EC2 account
    sts_connection = boto3.client('sts')
    assume_role = sts_connection.assume_role(
        RoleArn=(f'arn:aws:iam::{account}:role/{switchRole}'),
        RoleSessionName="net_ddns_lambda"
    )
    access_key = assume_role['Credentials']['AccessKeyId']
    secret_key = assume_role['Credentials']['SecretAccessKey']
    session_token = assume_role['Credentials']['SessionToken']
    
    # create service using the assumed role credentials
    sts_ec2_resource = boto3.resource(
        'ec2',
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        aws_session_token=session_token,
    )
    sts_ec2_client = boto3.client(
        'ec2',
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        aws_session_token=session_token,
    )
    # Gather information about the resource
    if event['source'] == 'aws.tag':
        try:
            instance_id = str.split(str(event['resources']), '/')[1][:19]
            instance = sts_ec2_client.describe_instances(InstanceIds=[instance_id])
            # compare tag event against instance uptime
            validate_tag_event(instance, event)
        except ClientError as e:
            print(e.response['Error']['Message'])

    if event['source'] == 'aws.ec2':
        try:
            instance_id = event['detail']['instance-id']
            instance = sts_ec2_client.describe_instances(InstanceIds=[instance_id])
        except ClientError as e:
            print(e.response['Error']['Message'])

    # Remove response metadata from the response
    instance.pop('ResponseMetadata')
    # Remove null values from the response. 
    instance = remove_empty_from_dict(instance)
    instance_dump = json.dumps(instance,default=json_serial)
    instance_attributes = json.loads(instance_dump)
    state = instance['Reservations'][0]['Instances'][0]['State']['Name']

    # Get Resource Inventory
    """This table is used to keep an inventory of instances and their attributes.  
    When an instance is terminated, its VPC and NetworkInterfaces attributes 
    are no longer available, so they have to be fetched from the table."""
    tables = dynamodb_client.list_tables()
    if 'DDNS' in tables['TableNames']:
        print('DynamoDB table already exists')
    else:
        create_table('DDNS')

    table = dynamodb_resource.Table('DDNS')

   # Update DB with Instance Attributes
    if state == 'running':
        try:
            instanceState = table.get_item(Key={'InstanceId': instance_id},AttributesToGet=['InstanceState'])
            instanceState.pop('ResponseMetadata')
            if instanceState:
                print(f'InstanceId: {instance_id} found in DDNS Table')
            else:
                print(f'InstanceId: {instance_id} not found in DDNS Table. Updating table...')
                table.put_item(
                    Item={
                        'InstanceId': instance_id,
                                         
                        'InstanceAttributes': instance_attributes
                    }
                )
                
        except ClientError as e:
            print(e.response['Error']['Message'])

    else:
        instance = table.get_item(
        Key={
            'InstanceId': instance_id
        },
        AttributesToGet=[
            'InstanceAttributes'
            ]
        )
        instance = instance['Item']['InstanceAttributes']

    #DNS Resource and VPC Tag Validation
    """Validationg Resource and VPC ahead of any other work """
    vpc_id = instance['Reservations'][0]['Instances'][0]['VpcId']
    
    DNSName = get_dns_tag(instance, sts_ec2_client)
    if DNSName:
        #Generate Shared VPC List - VPC must have a tag Key:"Type"
        shared_vpcs = get_sharedvpc_list()

        #Did the resource come from a Shared VPC?
        if (vpc_id in shared_vpcs):
            print('Resource inside Shared VPC ID: '+ vpc_id)
        else:
            print('Resource is not inside Non-Shared VPC ID: ' + vpc_id)
            exit()
    else:
        print(f'Tag DNSName not found or invalid on ResourceId: {instance_id}')
        exit()

    #Get Resource Shared VPC Domain Zone and Reversed Lookup Zone
    """Supports VPC with multiple CIDR blocks!"""
    vpc_domain_zone, vpc_reverse_zone_list = get_vpc_domain(vpc_id)

    #Shared VPC Domain Zone and Reversed Lookup Zone Validation and Associton
    """This is used to make sure all tagged VPCs are associated with all DDNS Zones"""
    vpc_domain_configuration(vpc_domain_zone, vpc_id, region, shared_vpcs)
    for vpc_zone in vpc_reverse_zone_list:
        vpc_domain_configuration(vpc_zone, vpc_id, region, shared_vpcs)

    #Instance A and PTR
    # Get instance attributes
    private_ip = instance['Reservations'][0]['Instances'][0]['PrivateIpAddress']
    reversed_ip_address = reverse_list(private_ip)
    # Set the reverse lookup zone
    reversed_lookup_name = reversed_ip_address + 'in-addr.arpa.'
    print ('The reverse lookup name for this instance is:', reversed_lookup_name)

    vpc_domain_zone_id = get_zone_id(vpc_domain_zone)
    print(vpc_domain_zone_id)
    vpc_zone_resources = route53.list_resource_record_sets(HostedZoneId=vpc_domain_zone_id)

    #Cleaning up zone resource records to aid search
    vpc_zone_resources.pop('ResponseMetadata')
    
    # create/delete A records and PTR records
    if state == 'running':
        for a in vpc_zone_resources['ResourceRecordSets']:
            fqdn = f'{DNSName}.{vpc_domain_zone}'
            if (fqdn == a['Name']):
         
                if (private_ip == a['ResourceRecords'][0]['Value']):
                    """This addresses the issue in case they had a valid DNSName, but changed it to something already used. but then decided to go back to the prior name"""
                    print(DNSName + ' already in Private Zone ' +  vpc_domain_zone + ' updating DDNS Tag')
                    sts_ec2_resource.create_tags(
                        Resources=[instance_id],
                        Tags=[{'Key':'DDNS', 'Value':DNSName + '.' + vpc_domain_zone}])
                else:
                    print(f'Please update DNSName Tag Value, {DNSName} is already taken')
                    sts_ec2_resource.create_tags(
                        Resources=[instance_id],
                        Tags=[{'Key':'DDNS', 'Value': f'Please update DNSName Tag Value, {DNSName} is already taken'}])
                    exit()
            else:
                print('DNSName tag value ' + DNSName + ' not in Private Zone ' +  vpc_domain_zone)
                """removes A record when before creating new A record. PRT is overwritten by the 'create_resource_record' function"""
                if (private_ip == a['ResourceRecords'][0]['Value']):
                    print(f"{a['Name'][:-(len(vpc_domain_zone))]} found on {vpc_domain_zone} with private ip {private_ip} deleting A Record")
                    Arecord = a['Name'][:-(len(vpc_domain_zone))]
                    delete_resource_record(vpc_domain_zone_id, Arecord, vpc_domain_zone, 'A', private_ip)

        try:
            create_resource_record(vpc_domain_zone_id, DNSName, vpc_domain_zone, 'A', private_ip)
            """PTR record cannot be created in the incorrect Reversed Lookup Zone. Which is good since we are
            looping through them - error sample: [RRSet with DNS name 202.0.168.10.in-addr.arpa. is not permitted in zone 178.10.in-addr.arpa.]
            This is primary used with a VPC has more than one CIDR block"""
            for vpc_reverse_zone in vpc_reverse_zone_list:
                vpc_reverse_zone_id = get_zone_id(vpc_reverse_zone)
                create_resource_record(vpc_reverse_zone_id, reversed_ip_address, 'in-addr.arpa', 'PTR', DNSName + '.' + vpc_domain_zone)
                
        except BaseException as e:
            print(e)
    
    else:
        print('deleting DNSName ' + DNSName + ' in Private Zone ' +  vpc_domain_zone )
        """If resource state is not 'running', A and PRT records must be deleted"""
        try:
            delete_resource_record(vpc_domain_zone_id, DNSName, vpc_domain_zone, 'A', private_ip)
            
            for vpc_reverse_zone in vpc_reverse_zone_list:
                vpc_reverse_zone_id = get_zone_id(vpc_reverse_zone)
                delete_resource_record(vpc_reverse_zone_id, reversed_ip_address, 'in-addr.arpa', 'PTR', DNSName + '.' + vpc_domain_zone)
                
        except BaseException as e:
            print(e)
            
    # Update DDNS Table with Instance Information
    if state == 'running':
        try:
            instanceHDNS = table.get_item(Key={'InstanceId': instance_id},AttributesToGet=['InstanceHDNS'])
            instanceHDNS.pop('ResponseMetadata')
            if instanceHDNS['Item']:
                #instanceHDNS['Item']['InstanceHDNS'].append({'DDNS': DNSName + '.' + vpc_domain_zone, 'DateTime': DateTime})
                HDNS = instanceHDNS['Item']['InstanceHDNS']
                HDNS.append({'DDNS': DNSName + '.' + vpc_domain_zone, 'DateTime': DateTime})
            else:
                HDNS = []
                HDNS.append({'DDNS': DNSName + '.' + vpc_domain_zone, 'DateTime': DateTime})
                
        except ClientError as e:
            print(e.response['Error']['Message'])
            
        update_table('DDNS', 'InstanceId', instance_id, 'InstanceDDNS', (DNSName + '.' + vpc_domain_zone))
        update_table('DDNS', 'InstanceId', instance_id, 'InstanceIp', private_ip)
        update_table('DDNS', 'InstanceId', instance_id, 'InstanceState', state)
        update_table('DDNS', 'InstanceId', instance_id, 'InstanceLastUpdate', DateTime)
        update_table('DDNS', 'InstanceId', instance_id, 'InstanceHDNS', HDNS)
        update_table('DDNS', 'InstanceId', instance_id, 'InstanceAttributes', instance_attributes)
        
         
        sts_ec2_client.create_tags(
            Resources=[instance_id], 
            Tags=[{'Key':'DDNS', 'Value':DNSName + '.' + vpc_domain_zone}])
                                                      
    # Update DynamoDB after deleting records, keep instance information for Historical Purposes
    if state != 'running':
        update_table('DDNS', 'InstanceId', instance_id, 'InstanceState', state)
        sts_ec2_client.delete_tags(
            Resources=[instance_id], 
            Tags=[{'Key':'DDNS'}])

#---- End of Lambda Function

def create_table(table_name):
    dynamodb_client.create_table(
            TableName=table_name,
            AttributeDefinitions=[
                {
                    'AttributeName': 'InstanceId',
                    'AttributeType': 'S'
                },
            ],
            KeySchema=[
                {
                    'AttributeName': 'InstanceId',
                    'KeyType': 'HASH'
                },
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 4,
                'WriteCapacityUnits': 4
            }
        )
    table = dynamodb_resource.Table(table_name)
    table.wait_until_exists()
    
def update_table(table_name, table_id, table_item, table_attr, attr_value):
    """This function updates subnet block."""
    table = dynamodb_resource.Table(table_name)
    try:
        response = table.update_item(
            Key={
                table_id: table_item,
            },
            # set somevalue = :letter,
            UpdateExpression="set %s = :g" % (table_attr),
            # ':letter': somevalue
            ExpressionAttributeValues={
                    ':g': attr_value
                },
            ReturnValues="UPDATED_NEW"
            )
        return response
    # Display an error if something goes wrong.
    except ClientError as e:
        return (e.response['Error']['Message'])
        
def create_resource_record(zone_id, host_name, hosted_zone_name, type, value):
    """This function creates resource records in the hosted zone passed by the calling function."""
    try:
        print ('Updating %s record %s in zone %s ' % (type, host_name, hosted_zone_name))
        if host_name[-1] != '.':
            host_name = host_name + '.'
        route53.change_resource_record_sets(
                    HostedZoneId=zone_id,
                    ChangeBatch={
                        "Comment": "Updated by EC2_TAG_DDNS_Lambda",
                        "Changes": [
                            {
                                "Action": "UPSERT",
                                "ResourceRecordSet": {
                                    "Name": host_name + hosted_zone_name,
                                    "Type": type,
                                    "TTL": 60,
                                    "ResourceRecords": [
                                        {
                                            "Value": value
                                        },
                                    ]
                                }
                            },
                        ]
                    }
                )

    except ClientError as e:
        print(e.response['Error']['Message'])
      
    return()

def delete_resource_record(zone_id, host_name, hosted_zone_name, type, value):
    """This function deletes resource records from the hosted zone passed by the calling function."""
    try:
        print('Deleting %s record %s in zone %s' % (type, host_name, hosted_zone_name))
        if host_name[-1] != '.':
            host_name = host_name + '.'
        route53.change_resource_record_sets(
                    HostedZoneId=zone_id,
                    ChangeBatch={
                        "Comment": "Updated by EC2_TAG_DDNS_Lambda",
                        "Changes": [
                            {
                                "Action": "DELETE",
                                "ResourceRecordSet": {
                                    "Name": host_name + hosted_zone_name,
                                    "Type": type,
                                    "TTL": 60,
                                    "ResourceRecords": [
                                        {
                                            "Value": value
                                        },
                                    ]
                                }
                            },
                        ]
                    }
                )

    except ClientError as e:
        print(e.response['Error']['Message'])
      
    return()
#
def create_zone(vpc_id, vpc_zone, region):
    """Create zone."""
    print('Creating zone %s' % vpc_zone)
    route53.create_hosted_zone(
        Name = vpc_zone,
        VPC = {
            'VPCRegion':region,
            'VPCId': vpc_id
        },
        CallerReference=str(uuid.uuid1()),
        HostedZoneConfig={
            'Comment': 'Updated by EC2_TAG_DDNS_Lambda',
        },
    )
#
def get_hosted_zone_properties(zone_id):
    hosted_zone_properties = route53.get_hosted_zone(Id=zone_id)
    hosted_zone_properties.pop('ResponseMetadata')
    return hosted_zone_properties   
#
def get_zone_id(zone_name):
    """This function returns the zone id for the zone name that's passed into the function."""
    if zone_name[-1] != '.':
        zone_name = zone_name + '.'
    hosted_zones = route53.list_hosted_zones()
    x = [record for record in hosted_zones['HostedZones'] if record['Name'] == zone_name]
    try:
        zone_id_long = x[0]['Id']
        zone_id = str.split(str(zone_id_long),'/')[2]
        return zone_id
    except:
        return None

def is_valid_hostname(DNSName):
    """This function checks to see whether the DNSName entered into the zone and cname tags is a valid DNSName."""
    if DNSName is None or len(DNSName) > 255:
        return False
    if DNSName[-1] == ".":
        DNSName = DNSName[:-1]
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in DNSName.split("."))

def get_dhcp_configurations(dhcp_options_id):
    """This function returns the names of the zones/domains that are in the option set."""
    zone_names = []
    ec2_resource = boto3.resource('ec2')
    dhcp_options = ec2_resource.DhcpOptions(dhcp_options_id)
    dhcp_configurations = dhcp_options.dhcp_configurations
    print('inside def - zone_names = %s' % dhcp_configurations)
    for configuration in dhcp_configurations:
        zone_names.append([x['Value'] + '.' for x in configuration['Values']])
        print('def for - zone_names = %s' % zone_names)
    return zone_names

def reverse_list(list):
    """Reverses the order of the instance's IP address and helps construct the reverse lookup zone name."""
    if (re.search('\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}',list)) or (re.search('\d{1,3}.\d{1,3}.\d{1,3}\.',list)) or (re.search('\d{1,3}.\d{1,3}\.',list)) or (re.search('\d{1,3}\.',list)):
        list = str.split(str(list),'.')
        list = [_f for _f in list if _f]
        
        list.reverse()
        reversed_list = ''
        for item in list:
            reversed_list = reversed_list + item + '.'
        return reversed_list
    else:
        print('Not a valid ip')
        exit()

def get_reversed_domain_prefix(subnet_mask, private_ip):
    """Uses the mask to get the zone prefix for the reverse lookup zone"""
    if 32 >= subnet_mask >= 24:
        third_octet = re.search('\d{1,3}.\d{1,3}.\d{1,3}.',private_ip)
        return third_octet.group(0)
    elif 24 > subnet_mask >= 16:
        second_octet = re.search('\d{1,3}.\d{1,3}.', private_ip)
        return second_octet.group(0)
    else:
        first_octet = re.search('\d{1,3}.', private_ip)
        return first_octet.group(0)

def create_reverse_lookup_zone(instance, reversed_domain_prefix, region):
    """Creates the reverse lookup zone."""
    print('Creating reverse lookup zone %s' % reversed_domain_prefix + 'in.addr.arpa.')
    route53.create_hosted_zone(
        Name = reversed_domain_prefix + 'in-addr.arpa.',
        VPC = {
            'VPCRegion':region,
            'VPCId': instance['Reservations'][0]['Instances'][0]['VpcId']
        },
        CallerReference=str(uuid.uuid1()),
        HostedZoneConfig={
            'Comment': 'Updated by EC2_TAG_DDNS_Lambda',
        },
    )

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError ("Type not serializable")

def remove_empty_from_dict(d):
    """Removes empty keys from dictionary. DynamoDB does not allow null values in a dict/JSON document"""
    if type(d) is dict:
        return dict((k, remove_empty_from_dict(v)) for k, v in d.items() if v and remove_empty_from_dict(v))
    elif type(d) is list:
        return [remove_empty_from_dict(v) for v in d if v and remove_empty_from_dict(v)]
    else:
        return d

def associate_zone(hosted_zone_id, region, vpc_id):
    """Associates private hosted zone with VPC"""
    route53.associate_vpc_with_hosted_zone(
        HostedZoneId=hosted_zone_id,
        VPC={
            'VPCRegion': region,
            'VPCId': vpc_id
        },
        Comment='Updated by EC2_TAG_DDNS_Lambda'
    )

def is_dns_hostnames_enabled(vpc):
    dns_hostnames_enabled = vpc.describe_attribute(
    DryRun=False,
    Attribute='enableDnsHostnames'
)
    return dns_hostnames_enabled['EnableDnsHostnames']['Value']

def is_dns_support_enabled(vpc):
    dns_support_enabled = vpc.describe_attribute(
    DryRun=False,
    Attribute='enableDnsSupport'
)
    return dns_support_enabled['EnableDnsSupport']['Value']

def get_hosted_zone_properties(zone_id):
    hosted_zone_properties = route53.get_hosted_zone(Id=zone_id)
    hosted_zone_properties.pop('ResponseMetadata')
    return hosted_zone_properties
    
def get_sharedvpc_list():
  """
  Get a list of all VPCs with tag 'Type' Value: 'SharedVPC'
  """
  shared_vpcs = []
  ec2_client = boto3.client('ec2')
  ec2_resource = boto3.resource('ec2')
  try:
      vpcs = ec2_client.describe_vpcs()['Vpcs']
      for vpc in vpcs:
          # handle vpc with no tags
          if 'Tags' in vpc:
              for tag in vpc['Tags']:
                  if 'Type' in tag.get('Key',{}):
                      if tag.get('Value') == vpcTypeTag:
                          shared_vpcs.append(vpc['VpcId'])
                          vpc_resource = ec2_resource.Vpc(vpc['VpcId'])
                          if is_dns_hostnames_enabled(vpc_resource):
                              print('DNS hostnames enabled for vpcId: %s' % vpc['VpcId'])
                          else:
                              print('DNS hostnames disabled for vpcId: %s.  Enabling DNS hostnames to use Route 53 private hosted zones.' % vpc['VpcId'])
                              ec2_client.modify_vpc_attribute(VpcId = vpc['VpcId'], EnableDnsHostnames = { 'Value': True })
    
                          if is_dns_support_enabled(vpc_resource):
                              print('DNS support enabled for vpcId: %s' % vpc['VpcId'])
                          else:
                              print('DNS support disabled for vpcId: %s.  Enabling DNS support to use Route 53 private hosted zones.' % vpc['VpcId'])
                              ec2_client.modify_vpc_attribute(VpcId = vpc['VpcId'], EnableDnsSupport = { 'Value': True })
                          
  except ClientError as e:
      print(e.response['Error']['Message'])
      
  return(shared_vpcs)
 
def get_dns_tag(instance, sts_ec2_client):
  """
  validate tags and DNSName tag exist
  """
  try:
      if "Tags" in instance['Reservations'][0]['Instances'][0]:
          tag = list(filter(lambda tag: tag['Key'] == dnsTag, instance['Reservations'][0]['Instances'][0]['Tags']))
          if tag:
              if is_valid_hostname(tag[0]['Value']):
                  dnsName = tag[0]['Value']
                  print('DNSName Tag found: %s' % dnsName)
              else:
                  dnsName = ('Invalid Character in DNSName Value: ' + tag[0]['Value'])
                  sts_ec2_client.create_tags(
                      Resources=[(instance['Reservations'][0]['Instances'][0]['InstanceId'])], 
                      Tags=[{'Key':'DDNS', 'Value': dnsName}])
                  print(dnsName)
                  return False
          else:
              print('ResourceId: ' + instance['Reservations'][0]['Instances'][0]['InstanceId'] + ' does not have a DNSName Tag')
              return False
      else:
          print('ResourceId: ' + instance['Reservations'][0]['Instances'][0]['InstanceId'] + ' does not any Tags')
          return False            
      
  except ClientError as e:
      print(e.response['Error']['Message'])
      
  return(dnsName)

def vpc_domain_configuration(vpc_zone, vpc_id, region, shared_vpcs):
    """Validate VPC Domain Zone and Reversed Lookup Zone exist and are associated with all SharedVPCs. If not, create Zones and associations"""
    print(vpc_id)
    hosted_zones = route53.list_hosted_zones()
    private_hosted_zones = [x for x in hosted_zones['HostedZones'] if x['Config']['PrivateZone'] is True]
    private_hosted_zone_collection = [x['Name'] for x in private_hosted_zones]
    if vpc_zone in private_hosted_zone_collection:
        print(f'Matcjomg VPC Zone: {vpc_zone} found in Route53 Zone Colletion')
        zone_id = get_zone_id(vpc_zone)
        zone_properties = get_hosted_zone_properties(zone_id)
        
        # validate shared_vpcs are associated with vpc_zone
        for vpcId in shared_vpcs:
            if vpcId in [x['VPCId'] for x in zone_properties['VPCs']]:
                print('Zone %s is associated with VPC %s' % (zone_id, vpcId))
            else:
                print('Associating Zone %s with VPC %s' % (zone_id, vpcId))
                try:
                    associate_zone(zone_id, region, vpcId)
                except BaseException as e:
                    print(e)
    else:
        print(f'No matching VPC zone: {vpc_zone} found in Route53 Zone Colletion, creating zone...')
        # create private hosted zone for reverse lookups
        create_zone(vpc_id, vpc_zone, region)
        zone_id = get_zone_id(vpc_zone)
        for vpcId in shared_vpcs:
            #def 'create_zone' associates vpc_id during creation
            if vpcId != vpc_id:
                associate_zone(zone_id, region, vpcId)
                print(f'Associated {vpcId} to Zone: {zone_id}')

#
def get_vpc_domain(vpc_id):
    """ Get VPC DNS Domain Zone and Generate Reversed Lookup Zone based on VPC CIDR block """
    vpc = ec2_resource.Vpc(vpc_id)
    vpc_domain_zone = ''
    vpc_reverse_zone_list = []
    try:
        dhcp_options_id = vpc.dhcp_options_id
        dhcp_configurations = get_dhcp_configurations(dhcp_options_id)
        vpc_domain_zone = dhcp_configurations[0][0]
        print(f'vpc_id: {vpc_id} vpc_domain_zone = {vpc_domain_zone}')
        
        if dhcp_configurations[1][0] == 'AmazonProvidedDNS.':
            print(f'vpc_domain_name servers = {dhcp_configurations[1][0]}')
        else:
            print(f'Invalid VPC Domain Name Servers = {dhcp_configurations[1][0]} Must be: AmazonProvidedDNS' )
            exit()

        ### Create Reversed Lookup Zone
        #vpc_cidr_blocks = [cidr['CidrBlock'] for cidr in vpc.cidr_block_association_set]
        vpc_cidr_blocks = [cidr['CidrBlock'] for cidr in vpc.cidr_block_association_set if cidr['CidrBlockState']['State'] == "associated"]
        for cidr in vpc_cidr_blocks:
            print(f'VPC CIDR {cidr}')
            vpc_cidr_ip = (cidr.split('/')[0])
            vpc_cidr_mask = int(cidr.split('/')[-1])
            vpc_reverse_zone_list.append(reverse_list(get_reversed_domain_prefix(vpc_cidr_mask, vpc_cidr_ip)) + 'in-addr.arpa.')

        print(f'vpc_id: {vpc_id} vpc_reverse_zone_list = {vpc_reverse_zone_list}')
        
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit()
        
    return(vpc_domain_zone, vpc_reverse_zone_list)

def validate_tag_event(instance, event):
    """When a new instance is launched with DNSName tag, two events are generated. 'ec2 running' and 'tag change'
    verify 'tag change' is from an instance that has been running for longer than 1min"""
    #get instance launch time and event time for comparison
    resource_datetime_str = str(instance['Reservations'][0]['Instances'][0]['LaunchTime'])[:-6]
    event_datetime_str = str(event['time'].replace('T', ' ')[:-1])
    # convert instance and event to timeformat
    resource_datetime = datetime.strptime(resource_datetime_str, '%Y-%m-%d %H:%M:%S')
    event_datetime = datetime.strptime(event_datetime_str, '%Y-%m-%d %H:%M:%S')
    # print date time variables
    print ('time differences: ' + str(event_datetime - resource_datetime))
    # convert to unix time
    event_time = time.mktime(event_datetime.timetuple())
    resource_time = time.mktime(resource_datetime.timetuple())
    print(f'event_time: {event_time} resource_time: {resource_time}')
    try: 
        if (event_time - resource_time) > 60.0:
            print ('Passed Tag Event Validation')
            return True
        else:
            print ('Tag event triggered by instance launch.. Exiting to avoid duplicate effort')
            exit()

    except ClientError as e:
        print(e.response['Error']['Message'])
