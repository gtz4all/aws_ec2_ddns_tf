resource "aws_cloudwatch_event_rule" "ec2_state_change" {
  name        = "ec2_state_change_event"
  description = "EC2 State Change DDNS"
  depends_on = [
    aws_lambda_function.ddns_lambda
  ]
  event_pattern = <<EOF
  {
    "source": ["aws.ec2"],
    "detail-type": ["EC2 Instance State-change Notification"],
    "detail": {
      "state": ["running", "terminated"]
    }
  }
EOF
}

resource "aws_cloudwatch_event_target" "ec2_state_change" {
  rule      = aws_cloudwatch_event_rule.ec2_state_change.name
  target_id = "ec2_state_change_target"
  arn       = aws_lambda_function.ddns_lambda.arn
}

resource "aws_cloudwatch_event_rule" "ec2_tag_change" {
  name        = "ec2_tag_change_event"
  description = "EC2 Tag Change DDNS"
  depends_on = [
    aws_lambda_function.ddns_lambda
  ]
  event_pattern = <<EOF
  {
    "source": ["aws.tag"],
    "detail-type": ["Tag Change on Resource"],
    "detail": {
      "changed-tag-keys": ["DNSName"],
      "service": ["ec2"],
      "resource-type": ["instance"]
    }
  }
EOF
}

resource "aws_cloudwatch_event_target" "ec2_tag_change" {
  rule      = aws_cloudwatch_event_rule.ec2_tag_change.name
  target_id = "ec2_tag_change_target"
  arn       = aws_lambda_function.ddns_lambda.arn
}

resource "aws_lambda_permission" "ec2_state_change" {
  statement_id = "AllowExecutionFromCloudWatchEC2Stage"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.ddns_lambda.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.ec2_state_change.arn}"
}

resource "aws_lambda_permission" "ec2_tag_change" {
  statement_id = "AllowExecutionFromCloudWatchEC2Tag"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.ddns_lambda.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.ec2_tag_change.arn}"
}