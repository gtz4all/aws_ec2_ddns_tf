output "ec2_tag_ddns_lambda_arn" {
  value = aws_lambda_function.ddns_lambda.arn
}

output "ec2_tag_ddns_invoke_arn" {
  value = aws_lambda_function.ddns_lambda.invoke_arn
}

output "ec2_tag_ddns_function_name" {
  value = aws_lambda_function.ddns_lambda.function_name
}
