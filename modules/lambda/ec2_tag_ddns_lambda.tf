# Archive a single file.

data "archive_file" "ec2_tag_ddns_lambda_zip" {
  type        = "zip"
  source_file = "${path.module}/ec2_tag_ddns.py"
  output_path = "ec2_tag_ddns_lambda.zip"
}

resource "aws_lambda_function" "ddns_lambda" {
  filename = "ec2_tag_ddns_lambda.zip"
  source_code_hash = "${data.archive_file.ec2_tag_ddns_lambda_zip.output_base64sha256}"
  function_name = "ec2_tag_ddns_lambda"
  role = var.ec2_tag_ddns_role_arn
  description = "EC2 Tagging Dynamic DNS"
  handler = "ec2_tag_ddns.lambda_handler"
  runtime = "python3.8"
  timeout = 360
  tags = {
    Name = "ec2_tag_ddns"
  }
}
