## Networking

This module is used to provide connectivity between VPCs and The transit Gateway. In addition, it updates the VPC route tables by pointing "10.0.0.0/16" towards the TGW attachment.

#### Single-VPC Networking 
* This is just to ilustrate what the module does when a single VPC is built. 
```mermaid
graph TD

subgraph A[Three Tier VPC1]
  subgraph B[Availability Zone 1]
    1[Public Subnet]
    2[Private Subnet]
    3[Local Subnet]
  end
  subgraph C[Availability Zone 2]
    4[Local Subnet]
    5[Private Subnet]
    6[Public Subnet]
  end
end

subgraph D[Transit Gateway]
  subgraph E[Multi-VPC Route Table]
    7[VPC1 Attachment] --> 3
    7[VPC1 Attachment] --> 4
  end
end

style 1 fill:#f5d5d5
style 2 fill:#e0ebff
style 3 fill:#d8e6d8
style 6 fill:#f5d5d5
style 5 fill:#e0ebff
style 4 fill:#d8e6d8
style A fill:#f7edd0
style D fill:#f5f5f5
```

#### Multi-VPC Networking
* As more VPCs get added, this module will repeat the above process for each one.

```mermaid
graph TD

subgraph A[Three Tier VPC1]
  1[Availability Zone 1]
  2[Availability Zone 2]
end
subgraph B[Three Tier VPC2]
  3[Availability Zone 1]
  4[Availability Zone 2]
end
subgraph C[Three Tier VPC3]
  5[Availability Zone 1]
  6[Availability Zone 2]
end

subgraph D[Transit Gateway]
  subgraph E[Multi-VPC Route Table]
    7[VPC1 Attachment] --> 1
    7[VPC1 Attachment] --> 2
    8[VPC2 Attachment] --> 3
    8[VPC2 Attachment] --> 4
    9[VPC3 Attachment] --> 5
    9[VPC4 Attachment] --> 6
  end
end

style A fill:#f7edd0
style B fill:#f7edd0
style C fill:#f7edd0
style D fill:#d8e0e6
style 1 fill:#d8e6d8
style 2 fill:#d8e6d8
style 3 fill:#d8e6d8
style 4 fill:#d8e6d8
style 5 fill:#d8e6d8
style 6 fill:#d8e6d8
```

###### VPC Route Table Update
<table>
<tr>
<th>public_rt</th>
<th>private_rt</th>
<th>local_rt</th>
</tr>
<tr>
<td>

Destination|Target
-----------|------
10.168.0.0/16|local
10.0.0.0/8|tgw
0.0.0.0|igw

</td>
<td>

Destination|Target
-----------|------
10.168.0.0/16|local
10.0.0.0/8|tgw


</td>
<td>

Destination|Target
-----------|------
10.168.0.0/16|local
10.0.0.0/8|tgw

</td>
</tr>
</table>


## Terraform Tip
* By default, terraform associates attachments to the Transit Gateway's Default Route table. So if I want to associate the attachment to another route table I would need to first disassociate it from the default route table. In order to avoid additional work,  I'm disabling this default setting.

```python
resource "aws_ec2_transit_gateway_vpc_attachment" "tgw" {
    transit_gateway_default_route_table_association = false
    transit_gateway_default_route_table_propagation = false
}
```
