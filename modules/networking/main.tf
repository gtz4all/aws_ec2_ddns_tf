/* Transit Gateway (tgw) VPC Attachments */
resource "aws_ec2_transit_gateway_vpc_attachment" "tgw" {
    transit_gateway_default_route_table_association = false
    transit_gateway_default_route_table_propagation = false
    
    subnet_ids         = [
        "${var.vpc_subnet0_id}", 
        "${var.vpc_subnet1_id}",
    ]
    transit_gateway_id = "${var.tgw_id}"
    vpc_id             = "${var.vpc_id}"
    tags = {
        Name = "${var.vpc_name}_tgw_attach"
    }
}

/* Transit Gateway (tgw) Route Domain: association and progation */
resource "aws_ec2_transit_gateway_route_table_association" "tgw" {
    transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw.id}"
    transit_gateway_route_table_id = "${var.tgw_rt_id}"
}
resource "aws_ec2_transit_gateway_route_table_propagation" "tgw" {
    transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw.id}"
    transit_gateway_route_table_id = "${var.tgw_rt_id}"
}

/* Add route table entry in VPC subnet route tables(rt) */
resource "aws_route" "public-tgw-route" {
    route_table_id = "${var.public_subnet_rt_id}"
    destination_cidr_block = "${var.dest_cidr_block}"
    transit_gateway_id = "${var.tgw_id}"
}
resource "aws_route" "private-tgw-route" {
    route_table_id = "${var.private_subnet_rt_id}"
    destination_cidr_block = "${var.dest_cidr_block}"
    transit_gateway_id = "${var.tgw_id}"
}
resource "aws_route" "local-tgw-route" {
    route_table_id = "${var.local_subnet_rt_id}"
    destination_cidr_block = "${var.dest_cidr_block}"
    transit_gateway_id = "${var.tgw_id}"
}
