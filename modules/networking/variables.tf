variable "dest_cidr_block" {
  description = "destination_cidr_block"
  type        = string
}

### TGW Variables
variable "tgw_id" {
  description = "tgw_id"
  type        = string
}
variable "tgw_rt_id" {
  description = "tgw_rt_id"
  type        = string
}

## VPC Variables
variable "vpc_id" {
  description = "vpc_id"
  type        = string
}
variable "vpc_name" {
  description = "vpc_name"
  type        = string
}

variable "vpc_subnet0_id" {
  description = "tgw_subnet0_id"
  type        = string
}
variable "vpc_subnet1_id" {
  description = "tgw_subnet1_id"
  type        = string
}

## RT Variables
variable "public_subnet_rt_id" {
  description = "public_subnet_rt_id"
  type        = string
}
variable "private_subnet_rt_id" {
  description = "public_subnet_rt_id"
  type        = string
}

variable "local_subnet_rt_id" {
  description = "public_subnet_rt_id"
  type        = string
}

