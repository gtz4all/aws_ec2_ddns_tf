## IAM
* This module is used to create roles and policies required for Lambda DDNS


```mermaid
graph TD
    A[ddns_lambda<br>core_ddns_lambda] -->|assume role| B(sts_ddns_lambda)
    B --> C{EC2}
    C -->|Get Tag Value| D[DNSName]
    C -->|Get VPC| E[SharedVPC]
    C -->|Route53| F[A/PTR Records]
    C -->|Update Tag| G[DDNS]
```


#### core-ddns-lambda-role

This is the role associated with the ec2_tag_ddns_lambda function
    * provides restricted access to ec2, dynamodb and route53 and sts

#### sts-ddns-lambda-role

This is the role is used by the ec2_tag_ddns_lambda function to gain access into the EC2 account which can either be in its own account or in a child account
    * provides restricted access to ec2 
    * this role can be replicated in other child accounts

## ec2-tag-restriction-policy

This policy should be associated with user accounts or roles. It denies the deletion of the DNSName tag once it is created. This helps maintain a stable Route53 environment. In addition it disable the creation or modification of the DDNS tag which is used by Lambda to interact with the users.

## Terraform Tip

[templatefile](https://www.terraform.io/docs/language/functions/templatefile.html) functionis very similar to python3's jinja2 templates.
```python
resource "aws_iam_role" "sts_ddns_role" {
  name      = "sts-ddns-lambda-role"
  assume_role_policy = templatefile("${path.module}/sts-ddns-role-trust-relationship.json", {
    core_ddns_role_arn = aws_iam_role.core_ddns_role.arn
  })
  depends_on             = [ aws_iam_role.core_ddns_role ]
}
```
The templatefile is invoked by providing the template file location ```templatefile("${path.module}/sts-ddns-role-trust-relationship.json", ```and passing variables  ```{core_ddns_role_arn = aws_iam_role.core_ddns_role.arn})```

```python
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "AWS": "${core_ddns_role_arn}"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
```
[Strings and Templates](https://www.terraform.io/docs/language/expressions/strings.html#string-templates): The template file uses interpolation to convert the variable into a string ```${core_ddns_role_arn}```
