## core DDNS Lambda Role
resource "aws_iam_role" "core_ddns_role" {
  name      = "core-ddns-lambda-role"
  assume_role_policy = file("${path.module}/core-ddns-role-trust-relationship.json")
}
resource "aws_iam_role_policy" "core_ddns_role_policy" {
  name   = "core-ddns-lambda-policy"
  role   = aws_iam_role.core_ddns_role.id
  policy = file("${path.module}/core-ddns-role-policy.json")
}

## Switch Role (sts) DDNS Lambda Role
## This role can be can be created on other accounts in a cross-account design
resource "aws_iam_role" "sts_ddns_role" {
  name      = "sts-ddns-lambda-role"
  assume_role_policy = templatefile("${path.module}/sts-ddns-role-trust-relationship.json", {
    core_ddns_role_arn = aws_iam_role.core_ddns_role.arn
  })
  depends_on             = [ aws_iam_role.core_ddns_role ]
}
resource "aws_iam_role_policy" "sts_ddns_role_policy" {
  name   = "sts-ddns-lambda-policy"
  role   = aws_iam_role.sts_ddns_role.id
  policy = templatefile("${path.module}/sts-ddns-role-policy.json", {
    core_ddns_role_arn = aws_iam_role.core_ddns_role.arn
  })
  depends_on             = [ aws_iam_role.core_ddns_role ]
}

## User DDNS Tag Restriction Policy
resource "aws_iam_policy" "ec2_ddns_tags_restriction_policy" {
  name   = "ec2_ddns_tags_restriction_policy"
  policy = file("${path.module}/ec2-tag-restriction-policy.json")
}
