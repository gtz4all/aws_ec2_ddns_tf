resource "aws_ec2_transit_gateway" "tgw" {
    tags = {
        Name = "${var.tgw_name}_tgw"
    }
}

# Transit Gateway (tgw1) Route Domain
resource "aws_ec2_transit_gateway_route_table" "tgw-rt" {
    transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
    tags = {
        Name = "${var.tgw_name}_tgw_rt"
    }
}
