## Transit Gateway

* A transit gateway is used to provide inter VPC communication. this module builds the Transit Gateway and one Route table

```mermaid
graph TD
subgraph D[Transit Gateway]
  subgraph E[Multi-VPC Route Table]
    7[Attachments] 
  end
end

style 7 fill:#e0ebff
style D fill:#f5f5f5
```
