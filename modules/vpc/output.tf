output "vpc_id" {
  value = aws_vpc.tf-vpc.id
}

output "public_subnet0_id" {
  value = aws_subnet.public-subnet[0].id
}
output "public_subnet1_id" {
  value = aws_subnet.public-subnet[1].id
}
output "public_rt_id" {
  value = aws_route_table.public-rt.id
}

output "private_subnet0_id" {
  value = aws_subnet.private-subnet[0].id
}
output "private_subnet1_id" {
  value = aws_subnet.private-subnet[1].id
}
output "private_rt_id" {
  value = aws_route_table.private-rt.id
}

output "local_subnet0_id" {
  value = aws_subnet.local-subnet[0].id
}
output "local_subnet1_id" {
  value = aws_subnet.local-subnet[1].id
}
output "local_rt_id" {
  value = aws_route_table.local-rt.id
}
