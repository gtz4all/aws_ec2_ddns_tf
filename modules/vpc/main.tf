data "aws_availability_zones" "available" {
  state = "available"
}

# Create a VPC
resource "aws_vpc" "tf-vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "${var.vpc_name}_vpc"
    Type = "SharedVPC"
  }
}

# Create DOPT
resource "aws_vpc_dhcp_options" "dopt" {
  domain_name          = "${var.vpc_domain}"
  domain_name_servers  = ["AmazonProvidedDNS"]
  tags = {
    Name = "${var.vpc_name}_vpc_dopt"
  }
}

resource "aws_vpc_dhcp_options_association" "dopt_association" {
  vpc_id          = aws_vpc.tf-vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.dopt.id
}

# Create Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.tf-vpc.id

  tags = {
    Name = "${var.vpc_name}_vpc_igw"
  }
}

#----------------------------------------------------------------------#
# Create Public Subnet
resource "aws_subnet" "public-subnet" {
  count                   = var.item_count
  vpc_id                  = aws_vpc.tf-vpc.id
  cidr_block              = [for cidr_block in cidrsubnets(var.vpc_cidr, 7, 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)][0][count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-${data.aws_availability_zones.available.names[count.index]}"
  }
}

# Create Private Subnet
resource "aws_subnet" "private-subnet" {
  count                   = var.item_count
  vpc_id                  = aws_vpc.tf-vpc.id
  cidr_block              = [for cidr_block in cidrsubnets(var.vpc_cidr, 7, 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)][1][count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false

  tags = {
    Name = "private-subnet-${data.aws_availability_zones.available.names[count.index]}"
  }
}

# Create local Subnet
resource "aws_subnet" "local-subnet" {
  count             = var.item_count
  vpc_id            = aws_vpc.tf-vpc.id
  cidr_block              = [for cidr_block in cidrsubnets(var.vpc_cidr, 7, 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)][2][count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "local-subnet-${data.aws_availability_zones.available.names[count.index]}"
  }
}

#----------------------------------------------------------------------#
# Create Public route table
resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.tf-vpc.id

  tags = {
    Name = "${var.vpc_name}_public_rt"
  }
}

# Create private route table
resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.tf-vpc.id

  tags = {
    Name = "${var.vpc_name}_private_rt"
  }
}

# Create local route table
resource "aws_route_table" "local-rt" {
  vpc_id = aws_vpc.tf-vpc.id

  tags = {
    Name = "${var.vpc_name}_local_rt"
  }
}

#----------------------------------------------------------------------#
# Create Public Subnet association with Public route table
resource "aws_route_table_association" "public-rt_association" {
  count          = var.item_count
  subnet_id      = aws_subnet.public-subnet[count.index].id
  route_table_id = aws_route_table.public-rt.id
}

# Create private Subnet association with private route table
resource "aws_route_table_association" "private-rt_association" {
  count          = var.item_count
  subnet_id      = aws_subnet.private-subnet[count.index].id
  route_table_id = aws_route_table.private-rt.id
}
# Create local Subnet association with local route table
resource "aws_route_table_association" "local-rt_association" {
  count          = var.item_count
  subnet_id      = aws_subnet.local-subnet[count.index].id
  route_table_id = aws_route_table.local-rt.id
}

#----------------------------------------------------------------------#
/* Add route table entry in VPC subnet route tables(rt) */
resource "aws_route" "public-igw-route" {
    route_table_id         = aws_route_table.public-rt.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id             = aws_internet_gateway.igw.id
    depends_on             = [aws_internet_gateway.igw, aws_route_table.public-rt, aws_route_table_association.public-rt_association, ]
}
