## Three Tier VPC
<div align="center">
![three_tier_vpc](/img/three_tier_vpc.jpg)
</div>

A Three Tier architecture is used to provide multiple security layers in order to provide an application's basic needs of compute, networking, storage and database. 
It consists of three layers: public, private and local. Each layer consists of a routing table with 2 subnets in different Availability Zones.
One of the goals of this design is to provide a secure structure to place application resources. Anything in the public layer is publicly
accessible but resources in the private and local layers are only accessible from inside the network. 

```mermaid
graph TD
subgraph A[Three Tier VPC1]
  subgraph B[Availability Zone 1]
    1[Public Subnet]
    2[Private Subnet]
    3[Local Subnet]
  end
  subgraph C[Availability Zone 2]
    4[Local Subnet]
    5[Private Subnet]
    6[Public Subnet]
  end
end
style 1 fill:#f5d5d5
style 2 fill:#e0ebff
style 3 fill:#d8e6d8
style 6 fill:#f5d5d5
style 5 fill:#e0ebff
style 4 fill:#d8e6d8
style A fill:#f7edd0
```

#### Public Layer 
```mermaid
graph TD
subgraph A[Three Tier VPC1]
  subgraph B[Availability Zone 1]
    1[Public Subnet]
  end
  subgraph C[Availability Zone 2]

    6[Public Subnet]
  end
end
style 1 fill:#f5d5d5
style 6 fill:#f5d5d5
style A fill:#f7edd0
```
This layer provides both egress/outbound and ingress/inbound internet access via an Internet Gateway (TGW). A resource in this layer needs a public IP associated with its network interface for it to be accessible from the internet. This public IP can from AWS’s dynamically assigned public range or from an Elastic IP.  Common resources in this layer are bastions hosts and Application Load Balancers

#### Private / Application Layer: 
```mermaid
graph TD
subgraph A[Three Tier VPC1]
  subgraph B[Availability Zone 1]
    2[Private Subnet]
  end
  subgraph C[Availability Zone 2]
    5[Private Subnet]
  end
end
style 2 fill:#e0ebff
style 5 fill:#e0ebff
style A fill:#f7edd0
```
This layer provides egress/outbound internet access via a NAT (Network Address Translation) Gateway. When a resource in this layer tries to reach the internet, its network interface private IP gets NATTed to the NAT Gateway Public IP. Common resources in this layer are web servers

#### Local / Database Layer: 
```mermaid
graph TD
subgraph A[Three Tier VPC1]
  subgraph B[Availability Zone 1]
    3[Local Subnet]
  end
  subgraph C[Availability Zone 2]
    4[Local Subnet]
  end
end
style 3 fill:#d8e6d8
style 4 fill:#d8e6d8
style A fill:#f7edd0
```
This layer does not have egress or ingress internet access. It’s primarily used to provide database and storage services to upper layers. 

## Terraform Tip

I'm using  Terraforms [cidrsubnets](https://www.terraform.io/docs/language/functions/cidrsubnets.html) to split the VPC CIDR block into smaller blocks, then further breaking those blocks into /24s

#### 2 AZs per VPC Tier
* ```vpc_cidr``` is passed by the root's main.tf. For this example, ```vcp_cidr``` == ```10.168.0.0/16```
```bash
[for cidr_block in cidrsubnets(var.vpc_cidr, 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)]
```
```mermaid
graph TD
subgraph A[10.168.0.0/16]
  subgraph B[16 + '7' = 23]
    1[23 + '1' = 24]
    2[23 + '1' = 24]
  end
  subgraph C[16 + '7' = 23]
    3[23 + '1' = 24]
    4[23 + '1' = 24]
  end
  subgraph D[16 + '7' = 23]
    5[23 + '1' = 24]
    6[23 + '1' = 24]
  end
end
style 1 fill:#f5d5d5
style 2 fill:#f5d5d5
style 3 fill:#e0ebff
style 4 fill:#e0ebff
style 5 fill:#d8e6d8
style 6 fill:#d8e6d8
```
###### terraform console
* taking three /23 CIDR blocks from the VPC CIDR block
```python
> [for cidr_block in cidrsubnets("10.168.0.0/16", 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)]
[
  tolist([
    "10.168.0.0/24",
    "10.168.1.0/24",
  ]),
  tolist([
    "10.168.2.0/24",
    "10.168.3.0/24",
  ]),
  tolist([
    "10.168.4.0/24",
    "10.168.5.0/24",
  ]),
]
> [for cidr_block in cidrsubnets("10.168.0.0/16", 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)][0]
tolist([
  "10.168.0.0/24",
  "10.168.1.0/24",
])
> [for cidr_block in cidrsubnets("10.168.0.0/16", 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)][1]
tolist([
  "10.168.2.0/24",
  "10.168.3.0/24",
])
> [for cidr_block in cidrsubnets("10.168.0.0/16", 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)][2]
tolist([
  "10.168.4.0/24",
  "10.168.5.0/24",
])
>
```

#### 4 AZs per VPC Tier

```python
[for cidr_block in cidrsubnets(var.vpc_cidr, 6, 6, 6) : cidrsubnets(cidr_block, 2, 2, 2, 2)]
```
```mermaid
graph TD
subgraph A[10.168.0.0/16]
  subgraph B[16 + '6' = 22]
    1[22 + '2' = 24]
    2[22 + '2' = 24]
    3[22 + '2' = 24]
    4[22 + '2' = 24]
  end
  subgraph C[16 + '6' = 22]
    5[22 + '2' = 24]
    6[22 + '2' = 24]
    7[22 + '2' = 24]
    8[22 + '2' = 24]
  end
  subgraph D[16 + '6' = 22]
    9[22 + '2' = 24]
    10[22 + '2' = 24]
    11[22 + '2' = 24]
    12[22 + '2' = 24]
  end
end
style 1 fill:#f5d5d5
style 2 fill:#f5d5d5
style 3 fill:#f5d5d5
style 4 fill:#f5d5d5
style 5 fill:#e0ebff
style 6 fill:#e0ebff
style 7 fill:#e0ebff
style 8 fill:#e0ebff
style 9 fill:#d8e6d8
style 10 fill:#d8e6d8
style 11 fill:#d8e6d8
style 12 fill:#d8e6d8
```

