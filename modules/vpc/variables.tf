# Function breaks var.vpc_cidr into 3 groups of 4 /24's - designed for 4 AZ / 3 Subnet groups
# [for cidr_block in cidrsubnets(var.vpc_cidr, 6, 6, 6) : cidrsubnets(cidr_block, 2, 2, 2, 2)]
# Brief Explanation: "10.168.0.0/16"  (16+"6"=22) = 3 groups of /22.  Then each group is then broken out in (22+"2" = 24 ) /24s 4 times
#
# Function breaks var.vpc_cidr into 3 groups of 2 /24's - designed for 2 AZ / 2 Subnet groups
# [for cidr_block in cidrsubnets(var.vpc_cidr, 7, 7, 7) : cidrsubnets(cidr_block, 1, 1)]
# Brief Explanation: "10.168.0.0/16"  (16+"7"=23) = 3 groups of /23.  Then each group is then broken out in (23+"1" = 24 ) /24s 2 times

#Count variable
variable "item_count" {
  description = "default count used to set AZs and instances"
  type        = number
  default     = 2
}
variable "vpc_cidr" {
  description = "custom vpc cidr block"
  type        = string
}
variable "vpc_name" {
  description = "custom vpc name"
  type        = string
}

variable "vpc_domain" {
  description = "custom vpc domain"
  type        = string
}
