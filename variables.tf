#VPC variables
variable "vpc_count" {
  description = "Number of VPCs"
  type        = number
  default     = 3
}

variable "vpc_cidrs" {
  description = "VPC CIDR Blocks"
  type    = list(string)
  default = ["10.128.0.0/16", "10.160.0.0/16", "10.192.0.0/16"]
}

variable "vpc_names" {
  description = "VPC Description"
  type    = list(string)
  default = ["prod", "test", "dev"]
}

variable "vpc_to_tgw_dest_cidr" {
  description = "vpc_to_tgw_dest_cidr"
  type    = string
  default = "10.0.0.0/8"
}
