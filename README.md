## AWS EC2 Dynamic DNS Solution
 
[[_TOC_]]
 
#### VPC DNS
 
DNS provides a friendly name to refer to an EC2's IP Address. AWS already provides [DNS support for their VPC](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-dns.html). However, they have some limitations:
 
* AWS-provided private DNS hostnames are static domain names that take the form ```ip-private-ipv4-address.ec2.internal``` for the ```us-east-1``` Region and , and ```ip-private-ipv4-address.region.compute.internal``` for other Regions. 
* These private DNS hostnames are only accessible between instances in the same VPC. In a multi-VPC environment, instances can't resolve the DNS hostname outside the VPC that they are in.
 
#### Route53
 
Route53 can be used to create custom private hosted zones. ```A private hosted zone is a container for one or more VPC Domain records.``` By creating a private hosted zone for a domain (such as gtz4all.com), and then creating records, instances can query Route 53 to find out how traffic should be routed for that domain within their VPC and among others.
 
#### Private Hosted Zone Requirements
 
For a private hosted zone to be accessible, there are multiple requirements.
for additional details, please refer to these links [Creating a private hosted zone]
* A VPC must be associated with the Private Hosted Zone
* The following VPC settings must be set to ```true```
    * ```enableDnsHostnames```
    * ```enableDnsSupport```
* DHCP options sets
    * Valid ```domain name`` (such as gtz4all.com)
    * domain-name-servers value: ```AmazonProvidedDNS```
 
for additional details, please refer to these links 
* [Creating a private hosted zone](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zone-private-creating.html)
* [DHCP options sets for your VPC](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_DHCP_Options.html)
 
#### Private Hosted Zone Records
 
*
* A(Address)record ``` A record``` maps a hostname and a domain name to IP addresses. The value inside an A record can only be an ```IP Address```
 
Name|Domain|A Record|Value
----|------|--------|-----
ec2lab|gtz4all.com|ec2lab.gtz4all.com|10.168.0.10
 
#### Reverse Lookup Zone Records
 
* A reverse lookup zone is used primarily to resolve IP addresses to Address records. Reverse lookup Zones use a special root domain called in-and.
* An IP address CIDR block reverse octet order is used to create a subdomain within the in-and.ARPA domain.
 
VPC CIDR Block|Reverse Lookup Domain
--------------|---------------------
10.168.0.0/16|168.10.in-addr.arpa.
 
* A DNS pointer record (PTR) provides the mapping of an IP address to a domain name. A DNS PTR record is the opposite of the 'A' record, which provides the IP address associated with a domain name.
 
IP Address|Reverse Lookup Zone|PTR Record|Value
----|------|--------|-----
10.168.0.10|168.10.in-addr.arpa.|10.0.168.10.in-addr.arpa.|ec2lab.gtz4all.com
 
 
## EC2 DDNS Lambda
![EC2 DDNS Lambda](img/single_account_dynamic_dns.jpg)
 
This solution uses tagging as a way to interact with the users. 
###### Overview
```mermaid
graph TD
    A[ddns_lambda<br>core_ddns_lambda] -->|assume role| B(sts_ddns_lambda)
    B --> C{EC2}
    C -->|Get Tag Value| D[DNSName]
    C -->|Get VPC| E[SharedVPC]
    C -->|Route53| F[A/PTR Records]
    C -->|Update Tag| G[DDNS]
```
 
1. the users create a tag key ``` DNSName``` with the ```value``` of their requested DNS name.
    * DNSName: ec2lab
    * VPC, Private IP, State
 
1. Lambda will pull resource DNSName Value to validate DNS Naming Standards
    * valid DNS name Characters
    * Lambda will update the tag ```DDNS``` to notify users if they provided invalid characters
 
 
1. Lambda will get the VPC attributes where the resource resides to validate if it is properly configured to support Route53.
    * ```enableDnsHostnames```, ```enableDnsSupport```
        * set to ```true``` if it isn't already
    * ```domain-name```  and ```AmazonProvidedDNS```
        *  ```domain name`` is later used to create Private Hosted Zone if it doesn't already exist
    * VPC CIDR Block
        *  ```VPC_CIDR_Block``` is used to generate Reverse Zone Lookup and create if required.
 
1. Lambda will then go into multiple phases to find out if ``` DNSName `` value already exists or if it's unique
    * Lambda will update tag ```DDNS``` to notify users if the provided ```DNSName``` value already exist 
    * if the provided ```DNSName``` value is unique, 
        * A Record is created in the VPC's ```domain name```
        * PTR Record based on the resource ```private ip``` is created in the ```VPC CIDR Block Reverse Lookup Zone```
 
1. DynamoDB DDNS
  * used to keep track of instance attributes and DNSName Changes
  * used to retrieve instance interface attributes when the instance has been terminated.
 
###### EC2 DDNS Lambda Flow
 
* ![EC2 DDNS Lambda Flow](/img/ddns_lambda_flow.jpg)
 
 
## Infrastructure as code
 
Infrastructure as code ( ```IaC```) solves several problems with manual and static infrastructure management. IaC provides the ability to create and destroy an environment multiple times. This provides a way to version control and create an environment lifecycle where environmental changes can transition from development, testing, and production.
 
#### Terraform
 
* Terraform is an IaC (Infrastructure as Code) tool used to build and modify infrastructure safely and efficiently. It keeps track of its environment state so rerunning the tool against a code with no changes will not generate any changes. 
 
#### Terraform Modules
 
* [Terraform modules](https://www.terraform.io/docs/language/modules/index.html) can be used to create reusable code using terraform loop-like features such as [count](https://www.terraform.io/docs/language/meta-arguments/count.html).
 
###### Terraform Module Structure
<div align="center">
![Terraform Design](/img/tf_structure.jpg)
</div>
 
#### VPC Module
 
* The VPC Module is used to build multiple Three Tier VPCs
 
```mermaid
graph TD
subgraph A[Three Tier VPC1]
  subgraph B[Availability Zone 1]
    1[Public Subnet]
    2[Private Subnet]
    3[Local Subnet]
  end
  subgraph C[Availability Zone 2]
    4[Local Subnet]
    5[Private Subnet]
    6[Public Subnet]
  end
end
style 1 fill:#f5d5d5
style 2 fill:#e0ebff
style 3 fill:#d8e6d8
style 6 fill:#f5d5d5
style 5 fill:#e0ebff
style 4 fill:#d8e6d8
style A fill:#f5f5f5
```
 
```python
module "vpc_build" {
  count     = var.vpc_count
  source    = "./modules/vpc"
  vpc_cidr  = var.vpc_cidrs[count.index]
  vpc_name  = var.vpc_names[count.index]
  vpc_domain  = "${var.vpc_names[count.index]}.gtz4all.com"
}
```
#### TGW Module
 
* This module builds the Transit Gateway and One route table 
```mermaid
graph TD
subgraph D[Transit Gateway]
  subgraph E[Multi-VPC Route Table]
    7[Attachments] 
  end
end
 
style 7 fill:#e0ebff
style D fill:#f5f5f5
```
 
```python
module "tgw" {
  source         = "./modules/tgw"
  tgw_name       = "multi_vpc"
}
```
 
#### Networking Module
 
* This module is used to create VPC to TGW Connectivity which results in all VPCs being able to communicate with each other.
 
```mermaid
graph TD
 
subgraph A[Three Tier VPC1]
  1[Availability Zone 1]
  2[Availability Zone 2]
end
subgraph B[Three Tier VPC2]
  3[Availability Zone 1]
  4[Availability Zone 2]
end
subgraph C[Three Tier VPC3]
  5[Availability Zone 1]
  6[Availability Zone 2]
end
 
subgraph D[Transit Gateway]
  subgraph E[Multi-VPC Route Table]
    7[VPC1 Attachment] --> 1
    7[VPC1 Attachment] --> 2
    8[VPC2 Attachment] --> 3
    8[VPC2 Attachment] --> 4
    9[VPC3 Attachment] --> 5
    9[VPC4 Attachment] --> 6
  end
end
 
style A fill:#f5f5f5
style B fill:#f5f5f5
style C fill:#f5f5f5
style D fill:#f5f5f5
style 1 fill:#d8e6d8
style 2 fill:#d8e6d8
style 3 fill:#d8e6d8
style 4 fill:#d8e6d8
style 5 fill:#d8e6d8
style 6 fill:#d8e6d8
```
* variables are being pass from the vpc and tgw to the root module which in turn passes those values to the networking module
```python
module "vpc_networking" {
  count     = var.vpc_count
  source          = "./modules/networking"
  dest_cidr_block = var.vpc_to_tgw_dest_cidr
  tgw_id          = module.tgw.tgw_id
  tgw_rt_id       = module.tgw.tgw_rt_id
 
  vpc_id          = module.vpc_build[count.index].vpc_id
  vpc_name        = var.vpc_names[count.index]
  vpc_subnet0_id  = module.vpc_build[count.index].local_subnet0_id
  vpc_subnet1_id  = module.vpc_build[count.index].local_subnet1_id
 
  public_subnet_rt_id  = module.vpc_build[count.index].public_rt_id
  private_subnet_rt_id = module.vpc_build[count.index].private_rt_id
  local_subnet_rt_id   = module.vpc_build[count.index].local_rt_id
}
```
 
#### IAM Module
 
* This module first creates the core_ddns_lambda role and policy. It then uses the core_ddns_lambda ARN to generate the sts_ddns_lambda policy which is associated with its corresponding role
 
* it also creates a [ec2_tag_restriction](/iam/ec2-tag-restriction-policy.json) policy
  * to maintain a healthy Route53 environment and be able to track Route53 Records correctly. Once the DNSName is created, it must remain for the lifetime of its resource. If the tag gets deleted, Lambda would not be able to perform route 53 clean up like deleted A and PTR records.
  * DDNS tag is used by lambda to update users on their DNS Name request. So users shouldn't be allowed to create or modify this tag.
```JSON
{
  "Statement": [
      {
          "Action": [
              "ec2:DeleteTags"
          ],
          "Condition": {
              "ForAllValues:StringEquals": {
                  "aws:TagKeys": [
                      "DNSName"
                  ]
              }
          },
          "Effect": "Deny",
          "Resource": "*",
          "Sid": "DenyDNSNameTagDeletion"
      },
      {
          "Action": [
              "ec2:DeleteTags",
              "ec2:CreateTags"
          ],
          "Condition": {
              "ForAllValues:StringEquals": {
                  "aws:TagKeys": [
                      "DDNS"
                  ]
              }
          },
          "Effect": "Deny",
          "Resource": "*",
          "Sid": "DenyDDNSTagChanges"
      }
  ],
  "Version": "2012-10-17"
}
```
```python
module "iam_roles" {
  source         = "./modules/iam"
}
```
 
#### Lambda Module
 
* This module builds a zip file which later is used to create the lambda function and its triggers.
  * Events are used to trigger the creation/deletion of Route53 resources
    * ```aws.ec2``` trigger is used to notify lambda about the creation or termination of an EC2. 
    * ```aws.tag``` trigger is used to notify lambda about the creation or modification of the "DNSName" tag. 
 
```python
module "ddns_lambda" {
  source         = "./modules/lambda"
  ec2_tag_ddns_role_arn = module.iam_roles.core_ddns_role_arn
}
```
 
#### EC2 Module
* This module is just to validate ec2_tag_ddns lambda environment
 
```python
module "ec2_ddns_lab" {
  count             = var.vpc_count
  source            = "./modules/ec2"
  vpc_id            = module.vpc_build[count.index].vpc_id
  public_subnet_id  = module.vpc_build[count.index].public_subnet0_id
  ec2_name          = "${var.vpc_names[count.index]}-ddns-lab"
}
```
 
## EC2 Tag DDNS Test Drive
 
#### Requirements
1. [Install Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
1.  Verify Terraform Installation
    * ```terraform -help```
1. [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html)
1. [Configuring the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)
1. Verify AWS CLI Access
    * ```aws ec2 describe-vpcs```
#### Building Infrastructure
1. terraform init
1. terraform plan
1. terraform apply
    ```python
    Do you want to perform these actions?
      Terraform will perform the actions described above.
      Only 'yes' will be accepted to approve.

      Enter a value: yes
    ```
1. Review the following Services
    * VPC
    * EC2
    * Route53
    * DynamoDB
    * Lambda
    * IAM
1. Update EC2 DNSName Value
    * review DDNS Tag Value
1. Terminate Instance
    * Review DDNS Table and Route53 Private Hosted Zone and Reverse Zone Lookup

#### Clean up

1. terraform destroy
    ```python
    Do you really want to destroy all resources?
      Terraform will destroy all your managed infrastructure, as shown above.
      There is no undo. Only 'yes' will be accepted to confirm.

      Enter a value: yes
    ```

1. Resources built by EC2_TAG_DDNS_Lambda are not destroyed, they will need to be deleted ```manually```
    * DynamoDB - DDNS
    * Private Hosted Zones
    * Reverse Lookup Zones

## Cross-Account EC2 Dynamic DNS 

* This design can be used across multiple accounts using the follow AWS Features:
  * [Shared VPCs](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-sharing.html)
  * [EventBridge Bus](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/CloudWatchEvents-CrossAccountEventDelivery.html)
  * [AssumeRole](https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRole.html)

###### EC2 DDNS Lambda Flow
 ![Cross Account EC2 DDNS Lambda Flow](/img/cross_account_dynamic_dns.jpg)
---

**NOTE**
Currently working on a Module to take care of the steps below.

---
 1. Share Subnets to a child account in your organization using [Resource Access Manager] (https://aws.amazon.com/blogs/networking-and-content-delivery/vpc-sharing-a-new-approach-to-multiple-accounts-and-vpc-management/) 
 1. Create AssumeRole (sts)... see [IAM Module STS Role](/modules/iam)
 1. Enable EventBus on ```Network Account```
 1. Create aws.ec2 and aws.tag [EventBridge Triggers](/modules/lambda/ec2_tag_event_trigger.tf) towards ```Network Account``` EventBus on ```Child Account```
 1. Create Instance on child account with DNSName Tag


