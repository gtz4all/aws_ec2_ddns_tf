
# #-------------------------Three-Tier VPC --------------------------------------#
# Building Three-Tier VPCs
module "vpc_build" {
  count     = var.vpc_count
  source    = "./modules/vpc"
  vpc_cidr  = var.vpc_cidrs[count.index]
  vpc_name  = var.vpc_names[count.index]
  vpc_domain  = "${var.vpc_names[count.index]}.gtz4all.com"
}

#-------------------------MUlti VPC Topology--------------------------------------#
# Building Transit Gateway
module "tgw" {
  source         = "./modules/tgw"
  tgw_name       = "multi_vpc"
}

# Building VPC to TGW Networking
module "vpc_networking" {
  count     = var.vpc_count
  source          = "./modules/networking"
  dest_cidr_block = var.vpc_to_tgw_dest_cidr
  tgw_id          = module.tgw.tgw_id
  tgw_rt_id       = module.tgw.tgw_rt_id

  vpc_id          = module.vpc_build[count.index].vpc_id
  vpc_name        = var.vpc_names[count.index]
  vpc_subnet0_id  = module.vpc_build[count.index].local_subnet0_id
  vpc_subnet1_id  = module.vpc_build[count.index].local_subnet1_id

  public_subnet_rt_id  = module.vpc_build[count.index].public_rt_id
  private_subnet_rt_id = module.vpc_build[count.index].private_rt_id
  local_subnet_rt_id   = module.vpc_build[count.index].local_rt_id
}

#-------------------------EC2 Tag Dynamic DNS-------------------------------------#
# Building EC2 DDNS IAM Roles
module "iam_roles" {
  source         = "./modules/iam"
}

# Building EC2 DDNS Lambda
module "ddns_lambda" {
  source         = "./modules/lambda"
  ec2_tag_ddns_role_arn = module.iam_roles.core_ddns_role_arn
}

# #-------------------------EC2  Dynamic DNS Demo-----------------------------------#
# Building EC2 Tagging - ec2_name = DDNSName Tag
module "ec2_ddns_lab" {
  count             = var.vpc_count
  source            = "./modules/ec2"
  vpc_id            = module.vpc_build[count.index].vpc_id
  public_subnet_id  = module.vpc_build[count.index].public_subnet0_id
  ec2_name          = "${var.vpc_names[count.index]}-ddns-lab"
}